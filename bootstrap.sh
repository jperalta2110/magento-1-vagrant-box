#!/usr/bin/env bash

SAMPLE_DATA=$1
INITIAL_INSTALL="true"
MAGE_VERSION="1.9.1.0"
DATA_VERSION="1.9.0.0"

# Git variables
GIT_NAME="Jorge"
GIT_EMAIL="jperalta0789@gmail.com"

SERVER_NAME="www.m1.local"


if [[ $INITIAL_INSTALL == "true" ]]; then
# Update Apt
# --------------------
apt-get update
apt-get upgrade

# For non-free MIBs
sudo apt-get install snmp -y
sudo apt-get install snmp-mibs-downloader -y

# Install Apache & PHP
# --------------------
apt-get install -y apache2
apt-get install -y php5
apt-get install -y libapache2-mod-php5
apt-get install -y php5-mysqlnd php5-curl php5-xdebug php5-gd php5-intl php-pear php5-imap php5-mcrypt php5-ming php5-ps php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc php5-xsl php-soap

php5enmod mcrypt

# Copy configuration files
  # apache conf
  sudo rm -rf /etc/apache2/apache2.conf
  sudo cp /vagrant/configuration/apache2.conf /etc/apache2/
  # xdebug
  sudo rm -rf  /etc/php5/apache2/conf.d/20-xdebug.ini
  sudo cp /vagrant/configuration/20-xdebug.ini /etc/php5/apache2/conf.d/
#------------------------------

# Add vagrant user to sudoers
#-------------------------
sudo usermod -aG sudo vagrant

# Delete default apache web dir and symlink mounted vagrant dir from host machine
# --------------------
rm -rf /var/www/html
mkdir /vagrant/httpdocs
ln -fs /vagrant/httpdocs /var/www/html

# Replace contents of default Apache vhost
# --------------------
VHOST=$(cat <<EOF
<VirtualHost *:80>
  ServerName $SERVER_NAME
  DocumentRoot "/var/www/html"
  Redirect permanent / https://$SERVER_NAME/
</VirtualHost>

<VirtualHost *:443>
  ServerName $SERVER_NAME
  DocumentRoot "/var/www/html"
  SSLEngine on
  SSLCertificateFile /vagrant/ssl/m1.local.crt
  SSLCertificateKeyFile /vagrant/ssl/m1.local.key
  <Directory "/var/www/html">
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
  </Directory>
</VirtualHost>
EOF
)

echo "$VHOST" > /etc/apache2/sites-enabled/000-default.conf


FHOST=$(cat <<EOF
127.0.0.1 $SERVER_NAME

# The following lines are desirable for IPv6 capable hosts
::1 ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
EOF
)


echo "$FHOST" > /etc/hosts

sudo a2enmod rewrite
sudo a2enmod ssl
service apache2 restart

# Mysql
# --------------------
# Ignore the post install questions
export DEBIAN_FRONTEND=noninteractive
# Install MySQL quietly
apt-get -q -y install mysql-server-5.5

mysql -u root -e "CREATE DATABASE IF NOT EXISTS magentodb"
mysql -u root -e "GRANT ALL PRIVILEGES ON magentodb.* TO 'magentouser'@'localhost' IDENTIFIED BY 'password'"
mysql -u root -e "FLUSH PRIVILEGES"

# Install Git
sudo apt-get install git -y
git config --global user.name $GIT_NAME
git config --global user.email $GIT_EMAIL

# Clean up
apt-get autoremove -y

# Magento
# --------------------
# http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/installing_magento_via_shell_ssh

# Download and extract
if [[ ! -f "/vagrant/httpdocs/index.php" ]]; then
  cd /vagrant/httpdocs
  git clone https://github.com/OpenMage/magento-mirror.git
  mv magento-mirror/* .
  mv magento-mirror/.htaccess .
  chmod -R o+w media var
  chmod o+w app/etc
  # Clean up downloaded file and extracted dir
  rm -rf magento-mirror*
  rm -rf .git/*
fi


# Sample Data
if [[ $SAMPLE_DATA == "true" ]]; then
  cd /vagrant

  if [[ ! -f "compressed-magento-sample-data-1.9.1.0.tgz" ]]; then
    # Only download sample data if we need to
    wget https://raw.githubusercontent.com/Vinai/compressed-magento-sample-data/1.9.1.0/compressed-magento-sample-data-1.9.1.0.tgz
  fi

  tar -xf compressed-magento-sample-data-1.9.1.0.tgz
  cp -R magento-sample-data-1.9.1.0/media/* httpdocs/media/
  cp -R magento-sample-data-1.9.1.0/skin/*  httpdocs/skin/
  mysql -u root magentodb < magento-sample-data-1.9.1.0/magento_sample_data_for_1.9.1.0.sql
  rm -rf magento-sample-data-1.9.1.0

fi


# Run installer
if [ ! -f "/vagrant/httpdocs/app/etc/local.xml" ]; then
  cd /vagrant/httpdocs
  sudo /usr/bin/php -f install.php -- --license_agreement_accepted yes \
  --locale en_US --timezone "America/New_York" --default_currency USD \
  --db_host localhost --db_name magentodb --db_user magentouser --db_pass password \
  --url "https://$SERVER_NAME" --use_rewrites yes \
  --use_secure yes --secure_base_url "https://$SERVER_NAME" --use_secure_admin yes \
  --skip_url_validation yes \
  --admin_lastname Owner --admin_firstname Store --admin_email "admin@example.com" \
  --admin_username admin --admin_password password123123
  /usr/bin/php -f shell/indexer.php reindexall
fi

echo "Install composer"
curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer


echo "Install n98-magerun"
cd /vagrant/httpdocs
wget https://files.magerun.net/n98-magerun.phar
php n98-magerun.phar

echo "Install useful Magento modules through Composer"
cd /vagrant/composer
composer update

echo "Install modman"
bash < <(wget -O - https://raw.github.com/colinmollenhour/modman/master/modman-installer)
source ~/.profile

fi

echo "Clear Cache and Reindex"
cd /vagrant/httpdocs
php n98-magerun.phar cache:enable
php n98-magerun.phar cache:flush
php n98-magerun.phar cache:clean
php n98-magerun.phar index:reindex:all

echo "Finished provisioning."