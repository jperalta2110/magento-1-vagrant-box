# Magento 1 Vagrant Box

A Magento environment provisioner for [Vagrant](http://www.vagrantup.com/).

![Magento & Vagrant](https://cookieflow.files.wordpress.com/2013/07/magento_vagrant.jpg?w=525&h=225)

* Creates a running Magento development environment with a few simple commands.
* Runs on Ubuntu (Trusty 14.04 64 Bit) \w PHP 5.5, MySQL 5.5, Apache 2.2
* Uses [Magento CE 1.9.1.0](http://www.magentocommerce.com/download)
* Automatically runs Magento's installer and creates CMS admin account.
* Optionally installs Magento Sample Store Inventory
* Automatically runs [n98-magerun](https://github.com/netz98/n98-magerun) installer.
* Installs [Modman](https://github.com/colinmollenhour/modman) - For easy module creation/modification.
* Installs [Xdebug](https://xdebug.org/) - For debugging issues with your prefered IDE.
* Installs [Composer](https://getcomposer.org/) - For installing dependecies.

## Getting Started

**Prerequisites**

* Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* Install [Vagrant](http://www.vagrantup.com/)
* Clone or [download](https://jp-nofraud@bitbucket.org/jp-nofraud/magento-1-vagrant-box.git) this repository to the root of your project directory `git clone https://jp-nofraud@bitbucket.org/jp-nofraud/magento-1-vagrant-box.git`
* In your project directory, run `vagrant up`

The first time you run this, Vagrant will download the bare Ubuntu box image. This can take a little while as the image is a few-hundred Mb. This is only performed once.

Vagrant will configure the base system before downloading Magento and running the installer.

## Set Variables in bootstrap sh
* GIT_NAME = Your Git Name
* GIT_EMAIL = Your Git email"
* SERVER_NAME = Name you want the virtualhost to have. detault = www.m1.local

## Usage
> NOTE: You must modify your host file on your local machine to point 192.168.33.10 to www.m1.local

* In your browser, head to `www.m1.local`
* Magento CMS is accessed at `www.m1.local/admin`
* User: `admin` Password: `password123123`
* Access the virtual machine directly using `vagrant ssh`
* When you're done `vagrant halt`

[Full Vagrant command documentation](http://docs.vagrantup.com/v2/cli/index.html)

## Sample Data

Sample data is automatically downloaded and installed by default. However, it's a reasonably large file and can take a while to download.

> "I don't want sample data"

Sample data installation can be disabled:

 * Open `Vagrantfile`
 * Change `sample_data = "true"` to `sample_data = "false"`
 * Run `vagrant up` as normal

## Need More?
> Contact me via email at jperalta0789@gmail.com